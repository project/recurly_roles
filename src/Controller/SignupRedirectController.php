<?php

namespace Drupal\recurly_roles\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Signup redirect controller for the recurly_roles module.
 */
class SignupRedirectController extends ControllerBase {

  /**
   * Redirects a user based on whether they're logged in.
   */
  public function signupRedirect() {
    if ($this->currentUser()->isAnonymous()) {
      return $this->redirect('recurly.select_plan');
    }
    else {
      return $this->redirect('recurly.redirect_to_registration');
    }
  }

}
